<?php

namespace Drupal\isp_ip\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\isp_ip\Entity\IspIpInterface;

/**
 * Class IspIpController.
 *
 *  Returns responses for IP routes.
 */
class IspIpController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a IP  revision.
   *
   * @param int $isp_ip_revision
   *   The IP  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($isp_ip_revision) {
    $isp_ip = $this->entityManager()->getStorage('isp_ip')->loadRevision($isp_ip_revision);
    $view_builder = $this->entityManager()->getViewBuilder('isp_ip');

    return $view_builder->view($isp_ip);
  }

  /**
   * Page title callback for a IP  revision.
   *
   * @param int $isp_ip_revision
   *   The IP  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($isp_ip_revision) {
    $isp_ip = $this->entityManager()->getStorage('isp_ip')->loadRevision($isp_ip_revision);
    return $this->t('Revision of %title from %date', ['%title' => $isp_ip->label(), '%date' => format_date($isp_ip->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a IP .
   *
   * @param \Drupal\isp_ip\Entity\IspIpInterface $isp_ip
   *   A IP  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(IspIpInterface $isp_ip) {
    $account = $this->currentUser();
    $langcode = $isp_ip->language()->getId();
    $langname = $isp_ip->language()->getName();
    $languages = $isp_ip->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $isp_ip_storage = $this->entityManager()->getStorage('isp_ip');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $isp_ip->label()]) : $this->t('Revisions for %title', ['%title' => $isp_ip->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all ip revisions") || $account->hasPermission('administer ip entities')));
    $delete_permission = (($account->hasPermission("delete all ip revisions") || $account->hasPermission('administer ip entities')));

    $rows = [];

    $vids = $isp_ip_storage->revisionIds($isp_ip);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\isp_ip\IspIpInterface $revision */
      $revision = $isp_ip_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $isp_ip->getRevisionId()) {
          $link = $this->l($date, new Url('entity.isp_ip.revision', ['isp_ip' => $isp_ip->id(), 'isp_ip_revision' => $vid]));
        }
        else {
          $link = $isp_ip->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.isp_ip.translation_revert', ['isp_ip' => $isp_ip->id(), 'isp_ip_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.isp_ip.revision_revert', ['isp_ip' => $isp_ip->id(), 'isp_ip_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.isp_ip.revision_delete', ['isp_ip' => $isp_ip->id(), 'isp_ip_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['isp_ip_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
