<?php

namespace Drupal\isp_ip\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the IP type entity.
 *
 * @ConfigEntityType(
 *   id = "isp_ip_type",
 *   label = @Translation("IP type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\isp_ip\IspIpTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\isp_ip\Form\IspIpTypeForm",
 *       "edit" = "Drupal\isp_ip\Form\IspIpTypeForm",
 *       "delete" = "Drupal\isp_ip\Form\IspIpTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\isp_ip\IspIpTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "type",
 *   admin_permission = "administer ip entities",
 *   bundle_of = "isp_ip",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/isp/ip/isp_ip/type/{isp_ip_type}",
 *     "add-form" = "/admin/isp/ip/isp_ip/type/add",
 *     "edit-form" = "/admin/isp/ip/isp_ip/type/{isp_ip_type}/edit",
 *     "delete-form" = "/admin/isp/ip/isp_ip/type/{isp_ip_type}/delete",
 *     "collection" = "/admin/isp/ip/isp_ip/type"
 *   }
 * )
 */
class IspIpType extends ConfigEntityBundleBase implements IspIpTypeInterface {

  /**
   * The IP type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The IP type label.
   *
   * @var string
   */
  protected $label;

}
