<?php

namespace Drupal\isp_ip;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\isp_ip\Entity\IspIpInterface;

/**
 * Defines the storage handler class for IP entities.
 *
 * This extends the base storage class, adding required special handling for
 * IP entities.
 *
 * @ingroup isp_ip
 */
class IspIpStorage extends SqlContentEntityStorage implements IspIpStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(IspIpInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {isp_ip_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {isp_ip_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(IspIpInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {isp_ip_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('isp_ip_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
