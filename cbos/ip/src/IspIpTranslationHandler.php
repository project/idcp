<?php

namespace Drupal\isp_ip;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for isp_ip.
 */
class IspIpTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
