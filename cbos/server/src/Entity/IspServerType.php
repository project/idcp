<?php

namespace Drupal\isp_server\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Server type entity.
 *
 * @ConfigEntityType(
 *   id = "isp_server_type",
 *   label = @Translation("Server type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\isp_server\IspServerTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\isp_server\Form\IspServerTypeForm",
 *       "edit" = "Drupal\isp_server\Form\IspServerTypeForm",
 *       "delete" = "Drupal\isp_server\Form\IspServerTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\isp_server\IspServerTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "isp_server_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "isp_server",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/isp/isp_server_type/{isp_server_type}",
 *     "add-form" = "/admin/isp/isp_server_type/add",
 *     "edit-form" = "/admin/isp/isp_server_type/{isp_server_type}/edit",
 *     "delete-form" = "/admin/isp/isp_server_type/{isp_server_type}/delete",
 *     "collection" = "/admin/isp/isp_server_type"
 *   }
 * )
 */
class IspServerType extends ConfigEntityBundleBase implements IspServerTypeInterface {

  /**
   * The Server type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Server type label.
   *
   * @var string
   */
  protected $label;

}
