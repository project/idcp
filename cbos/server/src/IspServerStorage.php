<?php

namespace Drupal\isp_server;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\isp_server\Entity\IspServerInterface;

/**
 * Defines the storage handler class for Server entities.
 *
 * This extends the base storage class, adding required special handling for
 * Server entities.
 *
 * @ingroup isp_server
 */
class IspServerStorage extends SqlContentEntityStorage implements IspServerStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(IspServerInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {isp_server_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {isp_server_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(IspServerInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {isp_server_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('isp_server_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}
