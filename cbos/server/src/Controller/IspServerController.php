<?php

namespace Drupal\isp_server\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\isp_server\Entity\IspServerInterface;

/**
 * Class IspServerController.
 *
 *  Returns responses for Server routes.
 */
class IspServerController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Server  revision.
   *
   * @param int $isp_server_revision
   *   The Server  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($isp_server_revision) {
    $isp_server = $this->entityManager()->getStorage('isp_server')->loadRevision($isp_server_revision);
    $view_builder = $this->entityManager()->getViewBuilder('isp_server');

    return $view_builder->view($isp_server);
  }

  /**
   * Page title callback for a Server  revision.
   *
   * @param int $isp_server_revision
   *   The Server  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($isp_server_revision) {
    $isp_server = $this->entityManager()->getStorage('isp_server')->loadRevision($isp_server_revision);
    return $this->t('Revision of %title from %date', ['%title' => $isp_server->label(), '%date' => format_date($isp_server->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Server .
   *
   * @param \Drupal\isp_server\Entity\IspServerInterface $isp_server
   *   A Server  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(IspServerInterface $isp_server) {
    $account = $this->currentUser();
    $langcode = $isp_server->language()->getId();
    $langname = $isp_server->language()->getName();
    $languages = $isp_server->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $isp_server_storage = $this->entityManager()->getStorage('isp_server');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $isp_server->label()]) : $this->t('Revisions for %title', ['%title' => $isp_server->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all server revisions") || $account->hasPermission('administer server entities')));
    $delete_permission = (($account->hasPermission("delete all server revisions") || $account->hasPermission('administer server entities')));

    $rows = [];

    $vids = $isp_server_storage->revisionIds($isp_server);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\isp_server\IspServerInterface $revision */
      $revision = $isp_server_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $isp_server->getRevisionId()) {
          $link = $this->l($date, new Url('entity.isp_server.revision', ['isp_server' => $isp_server->id(), 'isp_server_revision' => $vid]));
        }
        else {
          $link = $isp_server->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.isp_server.translation_revert', ['isp_server' => $isp_server->id(), 'isp_server_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.isp_server.revision_revert', ['isp_server' => $isp_server->id(), 'isp_server_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.isp_server.revision_delete', ['isp_server' => $isp_server->id(), 'isp_server_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['isp_server_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}
